import RNPgReactNativeSdk from "react-native-pg-react-native-sdk/bridge";


const UPI = "UPI";

export function startPayment(mode: string, appName: string, responseHandler) {
  let env = "TEST"; // "TEST" or "PROD"
  const checkout = new Map<string, string>();
  checkout.set("orderId", ""); // orderId here
  checkout.set("orderAmount", ""); // orderAmount here
  checkout.set("appId", ""); // apiKey here
  checkout.set("tokenData", ""); // cfToken here

  checkout.set("orderCurrency", "INR");
  checkout.set("orderNote", "Test Note");
  checkout.set("customerName", "Cashfree User");
  checkout.set("customerPhone", "9999999999");
  checkout.set("customerEmail", "cashfree@cashfree.com");
  checkout.set("hideOrderId", "true");
  checkout.set("color1", "#6002EE");
  checkout.set("color2", "#ffff1f");

  if (mode === UPI) {
    if (appName != null) {
      checkout.set("appName", appName);
    }
    RNPgReactNativeSdk.startPaymentUPI(checkout, env, responseHandler);
  } else {
    RNPgReactNativeSdk.startPaymentWEB(checkout, env, responseHandler);
  }
}
